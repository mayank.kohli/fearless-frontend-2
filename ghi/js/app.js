function createCard(name, description, pictureURL) {
    return `
    <div class="card">
        img src="{pictureURL}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">{name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">{description}</p>
        </div>
        <div class="card-footer">
            <small class="text-muted"?${starts} - ${ends}</small>    
        </div>
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            errorMessage();
        } else {
            const data = await response.json();
            
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                  const details = await detailResponse.json();
                  const name = details.conference.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;
                  const starts = new Date(details.conference.starts)
                  const newstarts = starts.toLocaleDateString()
                  const ends = new Date(details.conference.ends)
                  const newends = ends.toLocaleDateString()
                  const location = details.conference.location.name

                  const html = createCard(name, description, pictureUrl, location, newstarts, newends);
                  const row = document.querySelector('.row');
                  row.innerHTML += html;
                }
            }
        }
    } 
catch (error) {
    console.error('error', error);
  }
});


// window.addEventListener("DOMContentLoaded", async() => {
//     const states_url = 'http://localhost:8000/api/states/';
//     try {
//         const resp = await fetch(url)
//         if (!response.ok) {
//             throw new Error("This is an Error");
//         } else {
//             const states_data = await response.json();

//             const selectTag = document.querySelector('#state');
//             for (let state of data.states) {
//                 state = options
//             }
//         }
//     } catch (error) {
//         console.error('error', error);
//     }

// })